$(document).ready(function() {
	$('#cookieArea p').each(function(index, obj) {
		var showArea = $(obj).find('span:eq(1)');
		switch(index) {
		case 0:
			showArea.text($.cookie('child1'));
			break;
		case 1:
			showArea.text($.cookie('child2'));
			break;
		case 2:
			showArea.text($.cookie('parent'));
			break;
		case 3:
			showArea.text($.cookie('root'));
			break;
		}
	});
});

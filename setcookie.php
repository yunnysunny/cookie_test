<?php
/**
 * 设置如下hosts：
 * 127.0.0.1		www.a.com
 * 127.0.0.1		sub1.a.com
 * 127.0.0.1		sub2.a.com
 * 127.0.0.1		www.b.com
 * 通过www.a.com访问本文件
 * 
 */
date_default_timezone_set("PRC");
header("P3P: CP=\"CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR\"");//ie中需要设置一个p3p头，否则在跨域中获取不到cookie
setcookie('some','value',time()+60*60,'/','a.com');